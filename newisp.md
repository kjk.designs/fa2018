#New ISP

[FABISP](http://fab.cba.mit.edu/classes/863.16/doc/projects/ftsmin/index.html)


    1x ATtiny85
    2x 1kΩ resistors
    2x 499Ω resistors
    2x 49Ω resistors
    2x 3.3v zener diodes
    1x red LED
    1x green LED
    1x 100nF capacitor
    1x 2x3 pin header
![schematic](https://i.imgur.com/XkXMCe2.png)
![pcb](https://i.imgur.com/Y6aRAIJ.png)
![Board milled](https://i.imgur.com/m0uni9N.jpg)

![final build](https://i.imgur.com/NC0ZLds.jpg)

Solder joined the jumper for programming - a lil bit fuzzy apologies!
Doing this creates a bridge on the jumper near the ISP header (J1). This temporarily connects VCC to the Vprog pin on the ISP header so that the header can be used to program the tiny85. (The programmee supplies voltage on this pin and the programmer detects it). 
This will need to be removed once it is programmed to be a programmer.
![Imgur](https://i.imgur.com/6mPbMUh.jpg)

##Glamour shot
![Imgur](https://i.imgur.com/RHPc9nI.jpg)

##Programming
We have life!
![Imgur](https://i.imgur.com/HfxIHja.jpg)
Now to program it

following [these instructions](http://fab.cba.mit.edu/classes/863.16/doc/projects/ftsmin/windows_avr.html)
but using the atmel studio 7 found [here](https://www.microchip.com/avr-support/atmel-studio-7)
![Imgur](https://i.imgur.com/TTe3xWS.png)

Atmel studio doesnt appear to want to work on my laptop so back to the drawing board.
lets try adafruits program
[instructions](http://www.ladyada.net/learn/avr/setup-win.html)
![Imgur](https://i.imgur.com/QXmhyLH.png)

This didnt work, so tried arduino

I get a single blink on my board and this notice:

![Imgur](https://i.imgur.com/1UVaD90.png)

New attempt, trying Zadig
![Imgur](https://i.imgur.com/eE7K8p7.png)

nope, still getting issues. Back to the lab tomorrow to use a mac. or perhaps I should install linux on my pc...
time to clean up my drive a little so I have some space! Except the windows store isnt installed for some reason.. Ugh today is full of issues
[issue with others pcs](https://answers.microsoft.com/en-us/windows/forum/windows_10-windows_store-winpc/windows-10-store-not-installed/fe31de62-29b7-4d68-8284-d86ee489d2db)






