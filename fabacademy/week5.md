#Week 5: Electronics Production

<a href='https://photos.google.com/share/AF1QipNR_9q3EWjG7nH1aJnwXHMzacTKY79WrYOknLTeQfkoeoDED0wX28EogrZH7FIkLQ?key=UklQZHlEZ01CZEFFWlRnUzVTajNWRUpBZndGeEh3&source=ctrlq.org'><img src='https://lh3.googleusercontent.com/1_Ew7M7S2AgWcQPY5oJsx7_vu6smYrUhNHFeVAERxXqvy_dcbYZ-y4nD4qIhKRzv9tIL4mb34HDYedR-tLMDLYqRpQ-McAUzK2YFjHApEQdDS4Q_ZxSIdNhxAuMOqKVTaBuHUSvM4g' /></a>

##Previous Knowledge
I have some experience with circuit board design and population but never had the chance to actually make a board myself. This pig is on here too :-D

<a href='https://photos.google.com/share/AF1QipOB-70o9BNTcYTWajgFa7sVaHMIGdIB3y_e8TYsNV6-FPIu5nH2-fZ6o0PSNGZFSw?key=ODVUOHAxXzJHTGdmWXVwR0xYbVQzYVJnM21RWnBn&source=ctrlq.org'><img src='https://lh3.googleusercontent.com/z4SsrQWzfHYkMRQMCYiRT5UveIdKItzdFyU4-q1CI9cFNhRmDzGic3aJVvqazCNe7A9zyJ7cuOb_hvfRkTT6SeZRzXsHypyFHwAvCnB1MzXb9un3ECD50eqLuDRcbOi6UTqZz5r81Q' /></a> <a href='https://photos.google.com/share/AF1QipNwnfIot6u0B9REnxKo4-4OgriQcHwOUaqAMfIQCXYmt4dFnwUcWNZlpo-6YESh-w?key=b00tWmhBY0JIc2lZajFITTdUQ1dVMEV4a3JkVGNR&source=ctrlq.org'><img src='https://lh3.googleusercontent.com/YYeI6Xp7epVIMApWGEXELjSmbk61rnsABA2xq6tke3XZoMHZTHP0cn_06L-HdvnPk2UkBToI6q6qZTgLfFu0ZslzPGWTiJUy20YO7JiuBE-zwTVi3LRuWDdPgYfPBfMhNLDjklZDeA' /></a> <a href='https://photos.google.com/share/AF1QipOHKMqcWgv_XpbFOBFNGj414PdzXI6U_FSjUhEk0Q0PF6y_tDwCR0Mn7i-n2gkZiQ?key=ZVBjM0pOcEsxT0hJaG9SWXlJZWluWlkyU0hnMlJB&source=ctrlq.org'><img src='https://lh3.googleusercontent.com/P2UjAXerl1M4xWMwmMMcV7mBVXpNeuZm1A5DAJS9wP4pn6ed39zMhDbcaqc1MpIhOG8fEWq0N8bl-A53bQWrAgxuz5kjXwhxMS0E3WbUP0klAlwdmlNqeKbxYTe2PYivcqUeIrqoLQ' /></a> <a href='https://photos.google.com/share/AF1QipPdIT0Yg-q5QaIcdhUn6LnrMADPJ4O5lGHRhqRQQ4l-m-DzvH0KsKvYP-4IgoJC4g?key=Z3ZLVW1nd2QzRlJXSlk0LUd1c2ZRSTQxZmw4ejdB&source=ctrlq.org'><img src='https://lh3.googleusercontent.com/auN0a1w0ng_JACqark-WWmtJyI3G-jwyDGsLZN2_uN0YhvrPBlQGezaPTdCOP22WEq3Qhu8HAjUil6pL_t_69zB4z0Z4MTzEl689mDN-raA4563sJT5Y-Ol53tu1xkV8Es6x9J6Y5A' /></a>


##Tasks
Group Assignment
- [ ]  characterize the specifications of your PCB production process

Individual Assignment
- [ ] make an in-circuit programmer by milling the PCB
- [ ] optionally trying other processes
      
##Individual Assignment
###Step 1- Customising the ISP
From the Bueno ISP to the KJK ISP:
![Bueno](https://i.imgur.com/q4ny9Ddm.png)  ![KJK](https://i.imgur.com/gEv9ax6m.png)

###Step 2: Generating the file to be milled
Loading the file into fabtools
![1](https://imgur.com/0V2J6Em.png)

Selecting the mill as output
![2](https://imgur.com/deq2g6m.png)

Settings being configured to the group instructions
![3](https://imgur.com/CRwqxmy.png)

Weird toolpath when calculating
![4](https://imgur.com/sOgQe5w.png)

Trying some different settings to see if that helps
![5](https://imgur.com/UMFBb7P.png)

Ah! original export of the png was wrong, Illustrator output needs to be at 1000 dpi to be usable
![9](https://imgur.com/EdYvU2b.png)


Yay! looks ro have worked
![10](https://imgur.com/fdzR6Gh.png)

###Step 3 Milling the ISP
Board Cutting Timelapse
<iframe width="560" height="315" src="https://www.youtube.com/embed/-SesrKIESC0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Copper Board ![1](https://imgur.com/u0eqF53.png)
Overhanging Doublesided tape to attached ![2](https://imgur.com/VamFdEh.png)
Attaching the board to the sacrificial wax ![3](https://imgur.com/ybEI6QD.png)
Checking tool 2 - Which is the trace cutting tool is selected ![4](https://imgur.com/PU8K8v7.png)
Triple checking no residue is on the board after applying the board to the sacrificial wax ![5](https://imgur.com/quS5HGW.png)
setting the origin ![6](https://imgur.com/I3qERik.png)
Starting the cut ![7](https://imgur.com/qIPUuwM.png)
Trace cut on Vpanel ready to be loaded ![8](https://imgur.com/6aAaqke.png)
The Z0 Sensor ![9](https://imgur.com/s2l6PRA.png)
Attachment area for Z0 sensor ![10](https://imgur.com/xr4j96T.png)
Plugging in the Z0 Sensor ![11](https://imgur.com/fPMFynW.png)
Centering on the board ![12](https://imgur.com/o9tpeyg.png)
Z0 Sensing ![13](https://imgur.com/wp0hzf9.png)
Messed up board ![14](https://imgur.com/8MyaTRH.png)
Always check edge of the board! This tiny burr on the edge of the board completely messed up the trace cut ![15](https://imgur.com/DBu3WKU.png)
RML for the outline cut ![155](https://imgur.com/gJP9WZe.png)
Loading the File for the final Cut out ![16](https://imgur.com/KbyaWy6.png)
Changing to tool 1 for cutting out ![17](https://imgur.com/e0bgI6C.png)
Final Result ![18](https://imgur.com/eLtctha.png)

- - -

###Step 4 Populate the board

insert images

###Step 5 Programe the board
As I am using Windows, this caused concern for me
![1](https://imgur.com/PXEj537.png)
So I went on a hunt to see if I could program the board a different way. A cursory search led me to here:
[Arduino Attiny85 fabisp](http://forum.arduino.cc/index.php?topic=70841.0)
but considering this is not the same chip, I need to further research to see if this method would indeed work.
