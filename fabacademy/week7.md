#Week 7: Electronics Design

Fully embracing the fab learning environment I have decided this week to start off with things I do not know or have little experience with. One of these things is Eagle (these days I use Proteus which il be coming back to later), I have used the software in the past before it moved to Autodesk but I have little recollection of the inner workings of the software. 

Initial Impressions.... Clunky, Awkward, Run Away now!
Second Impression.... Arrgh.. Why cant I move this thing. This is rediculous.

After a bit more trying to get into this software, I give up for now - Risk of breaking my laptop was imminent, lets try something else: KiCAD. I have zero experience with this bit of software so I'm pretty excited.

Whilst KiCAD installs, Lets take a look at the base board
![Neil board](http://academy.cba.mit.edu/classes/embedded_programming/hello.ftdi.44.png)

I am interested in getting this board to work without the external XTAL as the internal 8mhz crystal should have enough juice to do anything that gets thrown at it at this level so I am excluding this for now. I see the purpose of including this for learning purposes but using a component where it is not necesary goes against my core beliefs and I cannot include it in good conscience.

| ID  | Component | Package | Value |
|-----|-----------|---------|-------|
| R1  | Resistor  |  1206   |  10k  |
| C1  | Capacitor |  1206   |  1uF  |
| IC1 | Integrated Circuit  | AVR |  Attiny44 |
|J1| ISP Header | 2x03 Header | N/A |
|J2| FTDI Header | 1x06 SIL Header | N/A|


During the install of KiCAD, the end screen intrigued me, I had already decided that I would render the board in proteus but if I am able to do this with KiCAD then awesome! [Wings3D](http://www.wings3d.com/)

![wings](https://imgur.com/zXnTli8.png)

Ok so, KiCAD and Wings3D installed, lets try this bit of kit out.

Moveable Components!! Already more of a fan

![kicad2](https://imgur.com/TBxZI92.png)

The component Library is pretty nice too
![complib](https://imgur.com/UObNXeD.png)

All the base components added to the schematic. A little concern with no package selection for resistors and caps, but hoping to find that out shortly.
![basecomps](https://imgur.com/2BfzEv8.png)

It appears that C1 is a decoupling capacitor as it sits between ground and VCC. A decoupling capacitor's job is to supress high-frequency noise in power supply signals. They take tiny voltage ripples, which could otherwise be harmful to delicate ICs, out of the voltage supply.

Without the XTAL There are so many pins available to use! Something does worry me, the pin outs appear to be different with rx/tx than on the neil board. It may simply be the component we are using is different and this wont cause an issue. But flagging it here as a possible issue

![conn](https://imgur.com/lXaoidu.png)

Unlike the schematic view being fairly intutitve, the pcb screen s a different kettle of fish

![kettle](https://imgur.com/WbGqZOe.png)

Ok, So wondering where to find the components, I realise I havent generated the netlist yet, Software I am used to auto does this so its a step I didnt consider

![gen](https://imgur.com/MJwy6Wl.png)

I knew the footprint attribute would come back to haunt me
![footprint](https://imgur.com/jwp2zAi.png)

Was pretty simple to fix, edit the component and browse footprint
![fp](https://imgur.com/E6zJ45E.png)

Some of the footprints are a bit off and autoroute isnt available anymore - not a major issue, just makes minor headaches dissapear. Do I stay with this software that functions more fluidly but appears limited, or do I move back to the infuriating Eagle and learn its idiosyncrises?

I really do not want to go back to Eagle as I am not a fan at all, so I am going to try and find a new library. I can get by without autorouting so thats not a factor.

Importing mods and libraries did not seem to work, I am afraid I caved and jumped to proteus for now, I dont call this defeat, just a reference point. I will figure out the other pieces of software

Schematic
![schem](https://imgur.com/uY3zVkR.png)
![board](https://imgur.com/7W1PrWK.png)

As you can see by the visalisation, the connectors are not ideal, but for machining this wont be a problem
![bv](https://imgur.com/jR5z8FW.png)

Simulating went fine after defining the internal clock

![sim2](https://imgur.com/aN5KkiX.png)

![sim](https://imgur.com/wAcSqcf.png)

To make sure I complied with this weeks assignment, I added in the xtal

![wx1](https://imgur.com/TPXiAIy.png)

![wx2](https://imgur.com/SPmzL9W.png)

![wx3](https://imgur.com/tkYDY7L.png)

 
 





