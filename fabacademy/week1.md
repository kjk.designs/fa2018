#Week 1: Principles and Practices
    
###Task: Come up with a final project idea
A few ideas percolated over the week, however with the final project of my 2nd year degree falling within a similar timeframe, I felt that the best move would be to incorporate that into my final project for the FabAcademy.

I aim to be tackling anxiety caused whilst travelling and one possible solution to that is for a system to be implemented that worked with GoogleMaps API tracking user location. There is already bus stop names in place on google maps, however if you are unfamiliar with an area, this does not help you recognise where you may need to press the bell on the bus to get off. 

There is scope for the project to grow through a wearable that would alert the user rather than an alert on a mobile phone. This could potentially be configured for set places.

The idea is still in its infancy and needs some fleshing out.


![travelassist](http://fa2018.gitlab.io/kjkdesigns/media/1x/Artboard%201.png)