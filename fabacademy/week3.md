#Week 3: CAD
    
###Task: model (raster, vector, 2D, 3D, render, animate, simulate, ...) a possible final project, and post it on your class page 

_ _ _

So for this week I took a slightly different route than would normally be ok for FabAcademy, but as this year is not the year I will be accredited, I thought it may be ok. I am also trying a new approach to documenting that I will alter previous weeks pages this coming weekend (06/02/2018)
	
_ _ _

I have Strong CAD skills thanks to my degree and my previous job, and took the opportunity to assist those who may need the help more than work on the assignment myself.

_ _ _

##01/02/2018: Recitation
After the local recitation I assisted Luiz with teaching Fusion360. This sharing my own skill was very enjoyable, and a very nice reminder of how much I enjoyed Fusion when I worked with it previously. It was cool showing the others the sculpt mode of Fusion especially!


_ _ _

##05/02/2018: Deadline Day / Missed Mondays Session	
This week so far has been a bit of tough one, not neceseraily on skills, but on time-management. My degree project had a big deadline that limited what I have been able to do so far. I am hoping to recitfy this over the next 2 days ready for the next project. I did however manage to help Sophie with her CAD learning with Fusion which was quite nice. I enjoyed hearing about her project.

_ _ _

##06/02/2018: Helping Jamie	
After lectures I went into the fablab all ready to sit down and get some work done, but ended up gravitating towards Jamie who was sketching out his project: 
_ _ _

<a href="http://fab.academany.org/2018/labs/fablabbrighton/students/james-moore/">Jamie Site</a> As he was hand sketching his design onto paper before planning to scan it in ad edit on Adobe Illustrator, I showed him the app AdobeCapture, which is this really cool app that lets you take a photo of real life items and sketches and automatically vectorise them. The following video does a great job of explaining it: 
_ _ _

<iframe width="560" height="315" src="https://www.youtube.com/embed/cfyeoWsfSG8?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
_ _ _


Result of his sketch grabbed by AdobeCapture: 
_ _ _

![jamie image](http://fa2018.gitlab.io/kjkdesigns/media/week%203/Jamie%20Help/adobe%20capture.jpeg)
_ _ _

He talked about finding this easier than directly sketching it into illustrator as te shape was somewhat complex. I explained that if you look at it from a geometry perspective, with illustator tools in mind, it was basically a rectangle with curved splines. Jamie then tried this out on illustrator:
_ _ _


![illustrator](http://fa2018.gitlab.io/kjkdesigns/media/week%203/Jamie%20Help/illustrator%20assist.jpeg)
_ _ _

Sadly I didnt take another photo after coming back an hour or so later, but he had managed to get everything into illustrator, using layers for the different parts to be laser cut. It was beautiful to see! I briefly talked to him about illustrators capabilities to turn it into 3D, but with time limitations of my own day I couldnt show him. Hopefully he figures this out, or I have a chance to demonstrate this later.

_ _ _


##06/02/2018: Helping Sophie
_ _ _

I got to work with Sophie <a href="http://fab.academany.org/2018/labs/fablabbrighton/students/sophie-kiarie/">(Sophies site)</a> again this evening, after she was having trouble with mirroring the button part of her game controller. I have permission to add the screenshots of the conversation from Sophie, but for now I will simply post the videos that I converted to GIFS that I used to assist her.


_ _ _



###Feb 7, 2018 5:09 AM : Need to add the rest to this
_ _ _

###Feb 7, 2018 12:10 PM : gifs not actually working now. Back to drawing board with those videos
_ _ _


#Feb 8, 2018 12:21 PM: Helping Andrew
_ _ _

Well this was a head scratcher!
_ _ _


![1](https://i.imgur.com/QLPrvBA.png)
_ _ _


Ok, this should be simple enough: nope!

![2](https://i.imgur.com/IzPo18q.png)

lets try something else...
![3](https://i.imgur.com/UI1GU8O.png)

ok, lets add some actualy geomotry to the problem
![4](https://imgur.com/EgtmFgq.png)

This is actually begining to bug me a little
![5](https://imgur.com/ORg3pDz.png)

Lets remove the units to see if that will work
![6](https://imgur.com/G1NyfXW.png)

##Fusion Fininickyness strikes again!
Working, but boy was that a pain. simply adding the *1mm to turn it into a unit that is usable
![7](https://imgur.com/GtyVfVi.png)



- - -

<br>
##Stirling Engine


<br>

<iframe width="560" height="315" src="https://www.youtube.com/embed/RTLm_G3ARqA" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/GS-AABbd_0c" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

