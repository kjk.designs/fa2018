#Week 4: Computer Controlled Cutting

##Task 1 group assignment:

characterize your lasercutter, making test part(s) that vary cutting settings and dimensions

 ##Task 2: individual assignment:

cut something on the vinylcutter

design, lasercut, and document a parametric press-fit construction kit, accounting for the lasercutter kerf, which can be assembled in multiple ways



###Checklist

- [ ] Group assignment

- [ ] Individual assignment



##07/02/2018 - Global Lecture Notes
[Journal Notes](../journal/laserkerf.md)


#Individual Task Pt 1

##Build Instructions
Cad Model
![1](https://imgur.com/4vCDiCP.png)

Rendered Cad Model
![render](https://imgur.com/N2FEJi3.png)

DXF parts
![2](https://imgur.com/W8eImZy.png)
CAD of interlocking parts
![3](https://imgur.com/A5pSjmI.png)


Build instructions
<iframe width="560" height="315" src="https://www.youtube.com/embed/sDTPGIlyenw" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>


##Hindsight
The construction model would use a lot of material, which I am quite concerned with.
perhaps a rethink
![rethink](https://imgur.com/eiYoEmm.png)

#New Week 4 Project
So because of timescale on the cutter, I decided to make a little box of shelves that could be scaled up and down as needed

![1](https://imgur.com/oeB8DUR.png)
![2](https://imgur.com/dvmNWn0.png)
![3](https://imgur.com/OnnvxBA.png)
![4](https://imgur.com/J4BgSxN.png)
![5](https://imgur.com/4n6wbGQ.png)
![6](https://imgur.com/3el90iv.png)
![7](https://imgur.com/a6z6euc.png)
![8](https://imgur.com/STaCC9l.png)
![9](https://imgur.com/sE8X7fQ.png)
![10](https://imgur.com/oRjHzOn.png)
![11](https://imgur.com/ajgnWHp.png)
![12](https://imgur.com/D3vgmWg.png)
![13](https://imgur.com/nPTAZPY.png)
![14](https://imgur.com/AE7fPYe.png)
![15](https://imgur.com/1UTYc1W.png)
Final Result (CAD)
![16](https://imgur.com/lBGOtPm.png)



