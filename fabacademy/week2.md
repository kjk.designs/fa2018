#Week 2:

##Step 3: use Git to push the site

```
kjkee@DESKTOP-U2IC20E MINGW64 /d/Adobe/Creative Cloud Files/GBfa2018 (master)
$ git status
On branch master
Your branch is up to date with 'origin/master'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        modified:   journal/my-misadventure-with-gitlab.md

no changes added to commit (use "git add" and/or "git commit -a")

kjkee@DESKTOP-U2IC20E MINGW64 /d/Adobe/Creative Cloud Files/GBfa2018 (master)
$ git stage .

kjkee@DESKTOP-U2IC20E MINGW64 /d/Adobe/Creative Cloud Files/GBfa2018 (master)
$ git commit -a -m "slashes"
[master 59ec49f] slashes
 1 file changed, 17 insertions(+)

kjkee@DESKTOP-U2IC20E MINGW64 /d/Adobe/Creative Cloud Files/GBfa2018 (master)
$ git push
warning: redirecting to https://gitlab.com/kjk.designs/fa2018.git/
Counting objects: 4, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (4/4), done.
Writing objects: 100% (4/4), 594 bytes | 297.00 KiB/s, done.
Total 4 (delta 2), reused 0 (delta 0)
To https://gitlab.com/kjk.designs/fa2018
   65d4e67..59ec49f  master -> master

kjkee@DESKTOP-U2IC20E MINGW64 /d/Adobe/Creative Cloud Files/GBfa2018 (master)
```


##iframe solving


###Initial code
```
<iframe src="https://trello.com/b/ji4xb99n/fl-week-4" frameBorder="0" width="340" height="220"></iframe>
```

###Attempt #
ok change the width to full
```
<iframe src="https://trello.com/b/ji4xb99n/fl-week-4" frameBorder="0" width="full" height="220"></iframe>
```

###Attemt #
100% maybe?
```
<iframe src="https://trello.com/b/ji4xb99n/fl-week-4" frameBorder="0" width="100%" height="300"></iframe>
```

### Attempt #
remove dims perhaps?
```
<iframe src="https://trello.com/b/ji4xb99n/fl-week-4" frameBorder="0"></iframe>
```


##Feb 8, 2018 1:18 PM
Added checklist to week 4
```
###Checklist
- [ ] Group assignment
- [ ] Individual assignment
```

Checklists can be ticked off with
```
###Checklist
- [x] Group assignment
- [ ] Individual assignment
```

Diplays like:
###Checklist
- [x] Group assignment
- [ ] Individual assignment


##Feb 8, 2018 1:41 PM
Learnt how to link to internal articles and have a reference for how to link to stuff in the same article :-)

```
[Jounral Notes](https://kjk.designs.gitlab.io/fa2018/journal/laserkerf.html)
[Journal Notes](/journal/laserkfer.md)
[journal notes](./journal/laserfer.md)
[Journal Notes](../journal/laserkerf.md)
```

its the 
```
[Journal Notes](../journal/laserkerf.md)
```

That links to articles in the gitbook.

I tried to link to another heading in an another article with this:
```
[iframe Solving](../fabacademy/week2.md##iframe solving)
```

however it didnt work. Resource 2 suggests using this:
```
 [link text](https://<username>.gitbooks.io/<bookname>/content/<chapter>/<article>.html#<heading-name>)
```
lets give it a go
[link check](../fabacademy/week2.md##Feb 8, 2018 1:41 PM)