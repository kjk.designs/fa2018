#Week 8 - Embedded Programming

##Individual Assignment
- [ ] Read a microcontroller data sheet
- [ ] program you board to do something
- [ ] play with multiple programming languages and environments

##Group Assignment
- [ ] Experiment with other architectures

##Previous Knowledge
###Local Recitation
###Python
Latent knowledge with python through work. Learning example as NDA on current projects
```

from tkinter import *
from tkinter import ttk
from tkinter import messagebox

class Feedback:

    def __init__(self, master):
        
        master.title('Explore California Feedback')
        master.resizable(False, False)
        master.configure(background = '#e1d8b9')
        
        self.style = ttk.Style()
        self.style.configure('TFrame', background = '#e1d8b9')
        self.style.configure('TButton', background = '#e1d8b9')
        self.style.configure('TLabel', background = '#e1d8b9', font = ('Arial', 11))
        self.style.configure('Header.TLabel', font = ('Arial', 18, 'bold'))      

        self.frame_header = ttk.Frame(master)
        self.frame_header.pack()
        
        self.logo = PhotoImage(file = 'tour_logo.gif')
        ttk.Label(self.frame_header, image = self.logo).grid(row = 0, column = 0, rowspan = 2)
        ttk.Label(self.frame_header, text = 'Thanks for Exploring!', style = 'Header.TLabel').grid(row = 0, column = 1)
        ttk.Label(self.frame_header, wraplength = 300,
                  text = ("We're glad you chose Explore California for your recent adventure.  "
                          "Please tell us what you thought about the 'Desert to Sea' tour.")).grid(row = 1, column = 1)
        
        self.frame_content = ttk.Frame(master)
        self.frame_content.pack()

        ttk.Label(self.frame_content, text = 'Name:').grid(row = 0, column = 0, padx = 5, sticky = 'sw')
        ttk.Label(self.frame_content, text = 'Email:').grid(row = 0, column = 1, padx = 5, sticky = 'sw')
        ttk.Label(self.frame_content, text = 'Comments:').grid(row = 2, column = 0, padx = 5, sticky = 'sw')
        
        self.entry_name = ttk.Entry(self.frame_content, width = 24, font = ('Arial', 10))
        self.entry_email = ttk.Entry(self.frame_content, width = 24, font = ('Arial', 10))
        self.text_comments = Text(self.frame_content, width = 50, height = 10, font = ('Arial', 10))
        
        self.entry_name.grid(row = 1, column = 0, padx = 5)
        self.entry_email.grid(row = 1, column = 1, padx = 5)
        self.text_comments.grid(row = 3, column = 0, columnspan = 2, padx = 5)
        
        ttk.Button(self.frame_content, text = 'Submit',
                   command = self.submit).grid(row = 4, column = 0, padx = 5, pady = 5, sticky = 'e')
        ttk.Button(self.frame_content, text = 'Clear',
                   command = self.clear).grid(row = 4, column = 1, padx = 5, pady = 5, sticky = 'w')

    def submit(self):
        print('Name: {}'.format(self.entry_name.get()))
        print('Email: {}'.format(self.entry_email.get()))
        print('Comments: {}'.format(self.text_comments.get(1.0, 'end')))
        self.clear()
        messagebox.showinfo(title = 'Explore California Feedback', message = 'Comments Submitted!')
    
    def clear(self):
        self.entry_name.delete(0, 'end')
        self.entry_email.delete(0, 'end')
        self.text_comments.delete(1.0, 'end')
         
def main():            
    
    root = Tk()
    feedback = Feedback(root)
    root.mainloop()
    
if __name__ == "__main__": main()

```
###RaspPi
Used the Pi for the stem kit at st as a replacement for the byvac board when units were getting debugged.

##Data sheets
[ATtiny Datasheet](http://academy.cba.mit.edu/classes/embedded_programming/doc8183.pdf)

[Xmega Datasheet](http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-8067-8-and-16-bit-AVR-Microcontrollers-ATxmega64A1-ATxmega128A1_Datasheet.pdf)

I decided to check out the xmega sheet as I was interested in the possibilty of the architectures possibility

##Arduino

I have been working with the arduino for my degree, last year I made the sunrise simulator clock, this year I am using the arduino to drive an el wire based bike light

```
/* 
 *  Arduino Code for illumi stained glass wake up clock
 */

// constant pins 
const int lpot1Pin = A0; // Analog input pin that the duration potentiometer is attached to
const int lpot2Pin = A1; // Analog input pin that the hours potentiometer is attached to
const int lpot3Pin = A2; // Analog input pin that the minutes potentiometer is attached to
const int lpot4Pin = A3; // AM/PM mod
const int ledPin = 9;    // LED connected to digital pin 9

// Pot raw and mapped variables
int lpot1Value = 0;   // variable to store the value coming from the pot
int lpot2Value = 00; //variable for hours time
int lpot3Value = 00; //variable for minutes time
int lpot4Value = 00; //variable for am
int lpot1dur = 0; // variable for mapped duration
int lpot2time = 0; // variable for mapped hours
int lpot3time = 0; // variable for mapped minutes
int lpot4ap = 0;


// Hours/minutes/seconds variables
int h=23; 
int m=59; 
int s=55; 

// Alarm hours/minutes/seconds variables
int alh=00;
int alm=00;
int als=00;

// interim seconds update after delay to keep clock inline
int stemp;


void setup() 
{ 
 Serial.begin(9600); 
} 


void loop() 
{ 
// displays time and mapped varials on serial for debugging purposes  
Serial.println(" ");
Serial.print("\TIME: "); 
Serial.print(h); 
Serial.print(":"); 
Serial.print(m); 
Serial.print(": ");
Serial.print(s);
if(h<12)Serial.print(" AM"); 
if(h>=12)Serial.print(" PM");
Serial.println(" Duration of alarm is set to ");
Serial.print(lpot1dur);  
Serial.print( "milliseconds");
Serial.println(" Time of alarm is set to ");
Serial.print(lpot2time);  
Serial.print(" : ");
Serial.print(lpot3time);
Serial.println(" clock or alarm function = ");
Serial.print(lpot4ap); 

delay(1000); 

// time update code
s=s+1;
  if(s>=60) { s=00; m=m+1; } 
  if(m==60) { m=00; h=h+1; } 
  if(h==24) { h=00;        } 




  
// alh hours pot setting
    lpot2Value = analogRead(lpot2Pin);
  // set map of ADC to 0-60 for duration 
  lpot2time = map(lpot2Value, 0, 1023, 0, 24);
  alh=(lpot2time);
     
     

// alm minutes pot setting
    lpot3Value = analogRead(lpot3Pin);
  // set map of ADC to 0-60 for duration 
  lpot3time = map(lpot3Value, 0, 1023, 0, 60);
  alm=(lpot3time);

  
  
  
// duration of alarm setting

    lpot1Value = analogRead(lpot1Pin);
  // set map of ADC to 0-60 for duration 
  lpot1dur = map(lpot1Value, 0, 1023, 0, 120);



 



   
}


```
<iframe width="560" height="315" src="https://www.youtube.com/embed/7EfoxCgxwgM?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

```
/*
  Fading EL Wire
*/

int ledPin = 9;    // LED connected to digital pin 9

void setup() {
  // nothing happens in setup
}

void loop() {
  // fade in from min to max in increments of 10 points:
  for (int fadeValue = 0 ; fadeValue <= 255; fadeValue += 10) {
    // sets the value (range from 0 to 255):
    analogWrite(ledPin, fadeValue);
    // wait for 30 milliseconds to see the dimming effect
    delay(30);
  }

  // fade out from max to min in increments of 10 points:
  for (int fadeValue = 255 ; fadeValue >= 0; fadeValue -= 10) {
    // sets the value (range from 0 to 255):
    analogWrite(ledPin, fadeValue);
    // wait for 30 milliseconds to see the dimming effect
    delay(30);
  }
}
```



