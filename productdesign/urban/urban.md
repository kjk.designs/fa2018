#BSc Product Design Second Year Project 3- Outside: Design for Urban Living'
#####Start Date: Tuesday 20th Feb 9:30am
#####Deadline Date: 22 May 4:30pm
#####Exhibition: 28th May - 1st June

- - -

##Brief

The brief for the next project is to design a small-scale product that will make daily life in the city better. As with the first L5 project, this is an individual project, where you will devise your own design solution to a problem you identify. However, you have been assigned groups, with each being allocated a specific location in Brighton to examine and design for . 

_ _ _

You have been assigned the location: Pavements and Roads

_ _ _

It is up to you to interpret exactly ‘where’ this location is/should be. You may decide to use one very specific place; you may visit several such spaces. As research preparation for this project you will need to visit the location(s) and spend some time there conducting observations (for at least an hour before Feb 20th, but the more you can do the better). 

_ _ _

You should observe carefully the PEOPLE who can be found in the location you have been given. What sort of people are they? Are they locals? Visitors? Are they old or young? Rich or poor? Look carefully; take notes, do sketches, record your observations. You then need to observe the PRACTICES or behaviours that are taking place in this location. Look carefully and curiously; take notes, do sketches, record your observations. Ask yourself ‘why do they do that…?’ This will serve to act as a basis for defining your problem area when we really get started. 

_ _ _

Thinking about your given location, when we begin the project you will need to choose one of the following briefs to pursue:

_ _ _

1. Develop a product that will improve the environmental impact of a practice in this location.

2. Develop a product that will make being in this location a more convivial experience.

3. Develop a product that will make participation in a practice more accessible in this location. 
