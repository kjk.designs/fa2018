# Kyle-James Keen

As a product design student, I tend to live in the school of Rams. However, for me, one rule supersedes Rams ideology:
<div style="text-align: center">	
"Good design is not decorative. Good design is problem solving"
</div>

<div style="text-align: right"> 
Jeffery Veen 
</div>	
																				
Which is why I have decided to try and find a solution to travel anxiety for my final project for FabAcademy 2018. Ideally this should coincide with my next degree project - Design for the Urban Environment.
 
Since May 2017, I have been posting my work to [Instagram](http://instagram.com/kjk.designs), which can be also be seen below: 


<!-- LightWidget WIDGET -->
<script src="//lightwidget.com/widgets/lightwidget.js"></script>
<iframe src="//lightwidget.com/widgets/40b5fac1662c52a6ae81722003587b9b.html" scrolling="no" allowtransparency="true" class="lightwidget-widget" style="width: 100%; border: 0; overflow: hidden;"></iframe>
