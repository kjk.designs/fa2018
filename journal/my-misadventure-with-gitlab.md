# Why I Moved to a Gitbook

So today I had to move from a standard website to a GitBook. I had been tempted to restart my website from scratch as my style of documentation had changed dramatically during week 3, and then an error that seemed completely unfixable happened and I just decided to abandon ship and try something else: GitBook!

##Errors Everywhere!
![Error1](https://scproxy-prod.adobecc.com/api?X-Location=https%3A%2F%2Fcc-eu1-prod.adobesc.com%2Fapi%2Fv1%2Fassets%2F26901dad-75ad-44e5-8c8c-00e9cbb26bc2%2Frenditions%2Fjpg%2F1200?&v=1517965527354&Authorization=Bearer%20eyJ4NXUiOiJpbXNfbmExLWtleS0xLmNlciIsImFsZyI6IlJTMjU2In0.eyJpZCI6IjE1MTgwMDA4NTExNDZfMmYyODI2ZmUtNTU5MC00MzVlLWJiNDEtNjI2ZTQ2N2ZhZGRjX3VlMSIsImNsaWVudF9pZCI6IkNyZWF0aXZlQ2xvdWRXZWIxIiwidXNlcl9pZCI6IjA1RTMxMzUyNTdGODFDRDQwQTQ5NUU1OEBBZG9iZUlEIiwidHlwZSI6ImFjY2Vzc190b2tlbiIsImFzIjoiaW1zLW5hMSIsImZnIjoiU0ZGUTVSVUZYUFA3T0ZRQUFBQUFBQUFBU0k9PT09PT0iLCJtb2kiOiJiMjU4Y2U0MSIsImMiOiJxR0pzeklRSnBhWG1ncWZCRC9HUUtRPT0iLCJleHBpcmVzX2luIjoiODY0MDAwMDAiLCJzY29wZSI6IkFkb2JlSUQsb3BlbmlkLGduYXYsY3JlYXRpdmVfY2xvdWQscmVhZF9vcmdhbml6YXRpb25zLGFkZGl0aW9uYWxfaW5mby5zY3JlZW5fbmFtZSxhZGRpdGlvbmFsX2luZm8uc2Vjb25kYXJ5X2VtYWlsLGFkZGl0aW9uYWxfaW5mby5yb2xlcyxzYW8uY2Nwcml2YXRlLHNhby5jY19leHRyYWN0LHNhby5jY2VfcHJpdmF0ZSxhY2NvdW50X3R5cGUsc2FvLmNjcHVibGlzaCIsImNyZWF0ZWRfYXQiOiIxNTE4MDAwODUxMTQ2In0.lI2KtI8Kz3xR-oUEu0Gy5LnA6YjeqxHUrmLwhFmx1TGZo425In6Ibco-OzhCQH4Yh8TGvn6m-83F6V0OI1Jj7-gM4kEJIbA9ekKnFq3YuC-2yXZ2tpbc9sIFZQF8rvmi5YJd9dpElbTmbo-8gxAjqQzJaZTIplFQ4DqG-YTz_YHtrCBDSENJt9Pj9bwiDY88HbrfFE91akDsQ2xRBDU5fCncjjAVw-j6PZr6Z57uNOIAFMt_ajPJDyHV6D1OLhj0zcxyGHpBOpJDXTnHTmwJyohizWNE6RS82bkqXy2E75dbzmOYGPpqb_irTPuK-a0nR4x7iwe3TsCr7nVxMnsmKg)

What is this lock file? It does not exist! Lets seek assistance

![error2](https://scproxy-prod.adobecc.com/api?X-Location=https%3A%2F%2Fcc-eu1-prod.adobesc.com%2Fapi%2Fv1%2Fassets%2Fa43fa1ab-669d-4396-84cf-11660cb0f58e%2Frenditions%2Fjpg%2F1200?&v=1517965542290&Authorization=Bearer%20eyJ4NXUiOiJpbXNfbmExLWtleS0xLmNlciIsImFsZyI6IlJTMjU2In0.eyJpZCI6IjE1MTgwMDA4NTExNDZfMmYyODI2ZmUtNTU5MC00MzVlLWJiNDEtNjI2ZTQ2N2ZhZGRjX3VlMSIsImNsaWVudF9pZCI6IkNyZWF0aXZlQ2xvdWRXZWIxIiwidXNlcl9pZCI6IjA1RTMxMzUyNTdGODFDRDQwQTQ5NUU1OEBBZG9iZUlEIiwidHlwZSI6ImFjY2Vzc190b2tlbiIsImFzIjoiaW1zLW5hMSIsImZnIjoiU0ZGUTVSVUZYUFA3T0ZRQUFBQUFBQUFBU0k9PT09PT0iLCJtb2kiOiJiMjU4Y2U0MSIsImMiOiJxR0pzeklRSnBhWG1ncWZCRC9HUUtRPT0iLCJleHBpcmVzX2luIjoiODY0MDAwMDAiLCJzY29wZSI6IkFkb2JlSUQsb3BlbmlkLGduYXYsY3JlYXRpdmVfY2xvdWQscmVhZF9vcmdhbml6YXRpb25zLGFkZGl0aW9uYWxfaW5mby5zY3JlZW5fbmFtZSxhZGRpdGlvbmFsX2luZm8uc2Vjb25kYXJ5X2VtYWlsLGFkZGl0aW9uYWxfaW5mby5yb2xlcyxzYW8uY2Nwcml2YXRlLHNhby5jY19leHRyYWN0LHNhby5jY2VfcHJpdmF0ZSxhY2NvdW50X3R5cGUsc2FvLmNjcHVibGlzaCIsImNyZWF0ZWRfYXQiOiIxNTE4MDAwODUxMTQ2In0.lI2KtI8Kz3xR-oUEu0Gy5LnA6YjeqxHUrmLwhFmx1TGZo425In6Ibco-OzhCQH4Yh8TGvn6m-83F6V0OI1Jj7-gM4kEJIbA9ekKnFq3YuC-2yXZ2tpbc9sIFZQF8rvmi5YJd9dpElbTmbo-8gxAjqQzJaZTIplFQ4DqG-YTz_YHtrCBDSENJt9Pj9bwiDY88HbrfFE91akDsQ2xRBDU5fCncjjAVw-j6PZr6Z57uNOIAFMt_ajPJDyHV6D1OLhj0zcxyGHpBOpJDXTnHTmwJyohizWNE6RS82bkqXy2E75dbzmOYGPpqb_irTPuK-a0nR4x7iwe3TsCr7nVxMnsmKg)

Login issue? What is going on?

![login issue](https://scproxy-prod.adobecc.com/api?X-Location=https%3A%2F%2Fcc-eu1-prod.adobesc.com%2Fapi%2Fv1%2Fassets%2F3e1f03d8-38b9-4aeb-bd3c-a3dc3e7023e4%2Frenditions%2Fjpg%2F1200?&v=1517965557578&Authorization=Bearer%20eyJ4NXUiOiJpbXNfbmExLWtleS0xLmNlciIsImFsZyI6IlJTMjU2In0.eyJpZCI6IjE1MTgwMDA4NTExNDZfMmYyODI2ZmUtNTU5MC00MzVlLWJiNDEtNjI2ZTQ2N2ZhZGRjX3VlMSIsImNsaWVudF9pZCI6IkNyZWF0aXZlQ2xvdWRXZWIxIiwidXNlcl9pZCI6IjA1RTMxMzUyNTdGODFDRDQwQTQ5NUU1OEBBZG9iZUlEIiwidHlwZSI6ImFjY2Vzc190b2tlbiIsImFzIjoiaW1zLW5hMSIsImZnIjoiU0ZGUTVSVUZYUFA3T0ZRQUFBQUFBQUFBU0k9PT09PT0iLCJtb2kiOiJiMjU4Y2U0MSIsImMiOiJxR0pzeklRSnBhWG1ncWZCRC9HUUtRPT0iLCJleHBpcmVzX2luIjoiODY0MDAwMDAiLCJzY29wZSI6IkFkb2JlSUQsb3BlbmlkLGduYXYsY3JlYXRpdmVfY2xvdWQscmVhZF9vcmdhbml6YXRpb25zLGFkZGl0aW9uYWxfaW5mby5zY3JlZW5fbmFtZSxhZGRpdGlvbmFsX2luZm8uc2Vjb25kYXJ5X2VtYWlsLGFkZGl0aW9uYWxfaW5mby5yb2xlcyxzYW8uY2Nwcml2YXRlLHNhby5jY19leHRyYWN0LHNhby5jY2VfcHJpdmF0ZSxhY2NvdW50X3R5cGUsc2FvLmNjcHVibGlzaCIsImNyZWF0ZWRfYXQiOiIxNTE4MDAwODUxMTQ2In0.lI2KtI8Kz3xR-oUEu0Gy5LnA6YjeqxHUrmLwhFmx1TGZo425In6Ibco-OzhCQH4Yh8TGvn6m-83F6V0OI1Jj7-gM4kEJIbA9ekKnFq3YuC-2yXZ2tpbc9sIFZQF8rvmi5YJd9dpElbTmbo-8gxAjqQzJaZTIplFQ4DqG-YTz_YHtrCBDSENJt9Pj9bwiDY88HbrfFE91akDsQ2xRBDU5fCncjjAVw-j6PZr6Z57uNOIAFMt_ajPJDyHV6D1OLhj0zcxyGHpBOpJDXTnHTmwJyohizWNE6RS82bkqXy2E75dbzmOYGPpqb_irTPuK-a0nR4x7iwe3TsCr7nVxMnsmKg)

Maybe its the SSH key getting a wierd result?
![ssh](https://scproxy-prod.adobecc.com/api?X-Location=https%3A%2F%2Fcc-eu1-prod.adobesc.com%2Fapi%2Fv1%2Fassets%2Fc8c634ff-0708-4b69-91b5-66a7bdf15951%2Frenditions%2Fjpg%2F1200?&v=1518004826882&Authorization=Bearer%20eyJ4NXUiOiJpbXNfbmExLWtleS0xLmNlciIsImFsZyI6IlJTMjU2In0.eyJpZCI6IjE1MTgwMDA4NTExNDZfMmYyODI2ZmUtNTU5MC00MzVlLWJiNDEtNjI2ZTQ2N2ZhZGRjX3VlMSIsImNsaWVudF9pZCI6IkNyZWF0aXZlQ2xvdWRXZWIxIiwidXNlcl9pZCI6IjA1RTMxMzUyNTdGODFDRDQwQTQ5NUU1OEBBZG9iZUlEIiwidHlwZSI6ImFjY2Vzc190b2tlbiIsImFzIjoiaW1zLW5hMSIsImZnIjoiU0ZGUTVSVUZYUFA3T0ZRQUFBQUFBQUFBU0k9PT09PT0iLCJtb2kiOiJiMjU4Y2U0MSIsImMiOiJxR0pzeklRSnBhWG1ncWZCRC9HUUtRPT0iLCJleHBpcmVzX2luIjoiODY0MDAwMDAiLCJzY29wZSI6IkFkb2JlSUQsb3BlbmlkLGduYXYsY3JlYXRpdmVfY2xvdWQscmVhZF9vcmdhbml6YXRpb25zLGFkZGl0aW9uYWxfaW5mby5zY3JlZW5fbmFtZSxhZGRpdGlvbmFsX2luZm8uc2Vjb25kYXJ5X2VtYWlsLGFkZGl0aW9uYWxfaW5mby5yb2xlcyxzYW8uY2Nwcml2YXRlLHNhby5jY19leHRyYWN0LHNhby5jY2VfcHJpdmF0ZSxhY2NvdW50X3R5cGUsc2FvLmNjcHVibGlzaCIsImNyZWF0ZWRfYXQiOiIxNTE4MDAwODUxMTQ2In0.lI2KtI8Kz3xR-oUEu0Gy5LnA6YjeqxHUrmLwhFmx1TGZo425In6Ibco-OzhCQH4Yh8TGvn6m-83F6V0OI1Jj7-gM4kEJIbA9ekKnFq3YuC-2yXZ2tpbc9sIFZQF8rvmi5YJd9dpElbTmbo-8gxAjqQzJaZTIplFQ4DqG-YTz_YHtrCBDSENJt9Pj9bwiDY88HbrfFE91akDsQ2xRBDU5fCncjjAVw-j6PZr6Z57uNOIAFMt_ajPJDyHV6D1OLhj0zcxyGHpBOpJDXTnHTmwJyohizWNE6RS82bkqXy2E75dbzmOYGPpqb_irTPuK-a0nR4x7iwe3TsCr7nVxMnsmKg)

Ok, lets try re doing the repo see what happens
![pipeine](https://scproxy-prod.adobecc.com/api?X-Location=https%3A%2F%2Fcc-eu1-prod.adobesc.com%2Fapi%2Fv1%2Fassets%2Fff555976-cc94-4b53-80b6-e4262f6ab9b8%2Frenditions%2Fjpg%2F1200?&v=1518004826862&Authorization=Bearer%20eyJ4NXUiOiJpbXNfbmExLWtleS0xLmNlciIsImFsZyI6IlJTMjU2In0.eyJpZCI6IjE1MTgwMDA4NTExNDZfMmYyODI2ZmUtNTU5MC00MzVlLWJiNDEtNjI2ZTQ2N2ZhZGRjX3VlMSIsImNsaWVudF9pZCI6IkNyZWF0aXZlQ2xvdWRXZWIxIiwidXNlcl9pZCI6IjA1RTMxMzUyNTdGODFDRDQwQTQ5NUU1OEBBZG9iZUlEIiwidHlwZSI6ImFjY2Vzc190b2tlbiIsImFzIjoiaW1zLW5hMSIsImZnIjoiU0ZGUTVSVUZYUFA3T0ZRQUFBQUFBQUFBU0k9PT09PT0iLCJtb2kiOiJiMjU4Y2U0MSIsImMiOiJxR0pzeklRSnBhWG1ncWZCRC9HUUtRPT0iLCJleHBpcmVzX2luIjoiODY0MDAwMDAiLCJzY29wZSI6IkFkb2JlSUQsb3BlbmlkLGduYXYsY3JlYXRpdmVfY2xvdWQscmVhZF9vcmdhbml6YXRpb25zLGFkZGl0aW9uYWxfaW5mby5zY3JlZW5fbmFtZSxhZGRpdGlvbmFsX2luZm8uc2Vjb25kYXJ5X2VtYWlsLGFkZGl0aW9uYWxfaW5mby5yb2xlcyxzYW8uY2Nwcml2YXRlLHNhby5jY19leHRyYWN0LHNhby5jY2VfcHJpdmF0ZSxhY2NvdW50X3R5cGUsc2FvLmNjcHVibGlzaCIsImNyZWF0ZWRfYXQiOiIxNTE4MDAwODUxMTQ2In0.lI2KtI8Kz3xR-oUEu0Gy5LnA6YjeqxHUrmLwhFmx1TGZo425In6Ibco-OzhCQH4Yh8TGvn6m-83F6V0OI1Jj7-gM4kEJIbA9ekKnFq3YuC-2yXZ2tpbc9sIFZQF8rvmi5YJd9dpElbTmbo-8gxAjqQzJaZTIplFQ4DqG-YTz_YHtrCBDSENJt9Pj9bwiDY88HbrfFE91akDsQ2xRBDU5fCncjjAVw-j6PZr6Z57uNOIAFMt_ajPJDyHV6D1OLhj0zcxyGHpBOpJDXTnHTmwJyohizWNE6RS82bkqXy2E75dbzmOYGPpqb_irTPuK-a0nR4x7iwe3TsCr7nVxMnsmKg)

I give in! New site, for the new style. 


- - -





#GitBook
I followed the instructions from here to get things working:

[Using gitbook with gitlab](https://axel-duerkop.de/blog/2016/03/15/using-gitbook-with-gitlab/)

##Step 1: Install node.js

![Site](images\journal\install node js.png) ![Site](images\journal\installing njs.png)
<img src ="images\journal\install node js.png"> <img src ="images\journal\installing njs.png">


installed node js. Win!

###Side note: Images are awkward in gitbooks
have tried both methods to display, could be the slash direction if im thinking about it properly
```
![Site](images\journal\install node js.png) ![Site](images\journal\installing njs.png)
<img src ="images\journal\install node js.png"> <img src ="images\journal\installing njs.png">

```

il try this and see if that works:
```
![Site](images/journal/install node js.png) ![Site](images/journal/installing njs.png)
<img src ="images/journal/install node js.png"> <img src ="images/journal/installing njs.png">
```

![Site](images/journal/install node js.png) ![Site](images/journal/installing njs.png)
<img src ="images/journal/install node js.png"> <img src ="images/journal/installing njs.png">

ok, still having issues! lets try using the full pushed http address

![Site](https://gitlab.com/kjk.designs/fa2018/tree/master/images/journal/install node js.png) ![Site](https://gitlab.com/kjk.designs/fa2018/tree/master/images/journal/installing njs.png)
<img src ="https://gitlab.com/kjk.designs/fa2018/tree/master/images/journal/install node js.png"> <img src ="https://gitlab.com/kjk.designs/fa2018/tree/master/images/journal/installing njs.png">

```
![Site](https://gitlab.com/kjk.designs/fa2018/tree/master/images/journal/install node js.png) ![Site](https://gitlab.com/kjk.designs/fa2018/tree/master/images/journal/installing njs.png)
<img src ="https://gitlab.com/kjk.designs/fa2018/tree/master/images/journal/install node js.png"> <img src ="https://gitlab.com/kjk.designs/fa2018/tree/master/images/journal/installing njs.png">
```

###Side note: Updated contents table figured out, even if I havent got images correct yet!
```
# Summary

* [Fab Academy 2018](README.md)
  * [Week 1: Principles & Practices]()
  * [Week 2: Project Management](fabacademy/week2.md)
  * [Week 3: CAD]()
* [Journal](journal.md)
  * [07/02/2018: My misadventure with GitLab](journal/my-misadventure-with-gitlab.md)


```
I have screenshots, but can I get them to work?! 
Also, im going to have to think of a way to view the gitbook without pushng every change. I suppose in time when all syntax is sorted I wont have to second guess myself!

####I may be a bit of an idiot. Imgs may not be displaying because I am forgetting the basic rules of defining width and height!

<img src="https://gitlab.com/kjk.designs/fa2018/tree/master/images/journal/install node js.png" width="100" height="100">

```
<img src="https://gitlab.com/kjk.designs/fa2018/tree/master/images/journal/install node js.png" width="100" height="100">
```

I decided to stop messing around and actually check out online how to do this. Lets see if it works. They say to do this:
```
![Local Image](./gitbook/images/<image_file_name>)
```
![Local Image](./images/journal/images yaya.png" width="100" height="100")

#RIGHT NOW I AM QUESTIONING MY DECISION TO GITBOOK!

another [site](https://www.gitbook.com/book/snowdream86/github-cheat-sheet/details) suggests this
```
![Alt Text](http://www.sheawong.com/wp-content/uploads/2013/08/keephatin.gif)
or 
[[ http://www.sheawong.com/wp-content/uploads/2013/08/keephatin.gif | height = 100px ]]
```

![Alt Text](http://www.sheawong.com/wp-content/uploads/2013/08/keephatin.gif)
[[ http://www.sheawong.com/wp-content/uploads/2013/08/keephatin.gif | height = 100px ]]

however, on reading the alts on the quick markdown viewer in my editor: Haroopad, I discovered this:
```
<img src='logo.png' alt='{alt text}' width='30px'>

```

So it is possible that my use of double quotes has messed me up:

<img src='https://gitlab.com/kjk.designs/fa2018/tree/master/images/journal/install node js.png' alt='{alt text}' width='30px'>

You know.. This could have something to do with the yml file setup and its plugins....

#Although! we have a winner

lets try this again!
![Alt Text](https://gitlab.com/kjk.designs/fa2018/tree/master/images/journal/install_node_js.png)

perhaps its to do with the file name?

I will try a new file and a web based file


![Alt Text](./images/winner.png)
![Alt Text](https://ih1.redbubble.net/image.374573832.5501/flat,800x800,075,f.jpg)
```
![Alt Text](./images/winner.png)
![Alt Text](https://ih1.redbubble.net/image.374573832.5501/flat,800x800,075,f.jpg)


```

So ok, its something to do with the way the gitbook handles local images then. For the minute I will host images somewhere and simply link to them, but I will solve this mystery!

I couldnt let it go, and wondered if the image was in the same directory, perhaps that would change things
![winner winner](/winner.png)
<img src='winner.png' alt='{alt text}' width='30px'>
Still no dice.


#FIXED IT!

![winner winner](https://gitlab.com/kjk.designs/fa2018/raw/master/images/winner.png)

```
![winner winner](https://gitlab.com/kjk.designs/fa2018/raw/master/images/winner.png)
```

- - -


#Right! back to where I wanted to be in the first place!

##Step 1: Install node.js

![site](https://scproxy-prod.adobecc.com/api?X-Location=https%3A%2F%2Fcc-eu1-prod.adobesc.com%2Fapi%2Fv1%2Fassets%2Fc6e279ce-d1ce-4738-89fa-16d5da0bbee0%2Frenditions%2Fjpg%2F1200?&v=1517969606252&Authorization=Bearer%20eyJ4NXUiOiJpbXNfbmExLWtleS0xLmNlciIsImFsZyI6IlJTMjU2In0.eyJpZCI6IjE1MTgwMDA4NTExNDZfMmYyODI2ZmUtNTU5MC00MzVlLWJiNDEtNjI2ZTQ2N2ZhZGRjX3VlMSIsImNsaWVudF9pZCI6IkNyZWF0aXZlQ2xvdWRXZWIxIiwidXNlcl9pZCI6IjA1RTMxMzUyNTdGODFDRDQwQTQ5NUU1OEBBZG9iZUlEIiwidHlwZSI6ImFjY2Vzc190b2tlbiIsImFzIjoiaW1zLW5hMSIsImZnIjoiU0ZGUTVSVUZYUFA3T0ZRQUFBQUFBQUFBU0k9PT09PT0iLCJtb2kiOiJiMjU4Y2U0MSIsImMiOiJxR0pzeklRSnBhWG1ncWZCRC9HUUtRPT0iLCJleHBpcmVzX2luIjoiODY0MDAwMDAiLCJzY29wZSI6IkFkb2JlSUQsb3BlbmlkLGduYXYsY3JlYXRpdmVfY2xvdWQscmVhZF9vcmdhbml6YXRpb25zLGFkZGl0aW9uYWxfaW5mby5zY3JlZW5fbmFtZSxhZGRpdGlvbmFsX2luZm8uc2Vjb25kYXJ5X2VtYWlsLGFkZGl0aW9uYWxfaW5mby5yb2xlcyxzYW8uY2Nwcml2YXRlLHNhby5jY19leHRyYWN0LHNhby5jY2VfcHJpdmF0ZSxhY2NvdW50X3R5cGUsc2FvLmNjcHVibGlzaCIsImNyZWF0ZWRfYXQiOiIxNTE4MDAwODUxMTQ2In0.lI2KtI8Kz3xR-oUEu0Gy5LnA6YjeqxHUrmLwhFmx1TGZo425In6Ibco-OzhCQH4Yh8TGvn6m-83F6V0OI1Jj7-gM4kEJIbA9ekKnFq3YuC-2yXZ2tpbc9sIFZQF8rvmi5YJd9dpElbTmbo-8gxAjqQzJaZTIplFQ4DqG-YTz_YHtrCBDSENJt9Pj9bwiDY88HbrfFE91akDsQ2xRBDU5fCncjjAVw-j6PZr6Z57uNOIAFMt_ajPJDyHV6D1OLhj0zcxyGHpBOpJDXTnHTmwJyohizWNE6RS82bkqXy2E75dbzmOYGPpqb_irTPuK-a0nR4x7iwe3TsCr7nVxMnsmKg)



![installing](https://scproxy-prod.adobecc.com/api?X-Location=https%3A%2F%2Fcc-eu1-prod.adobesc.com%2Fapi%2Fv1%2Fassets%2F552337f5-d358-4f07-b7a8-5aca220b359e%2Frenditions%2Fjpg%2F1200?&v=1517969644505&Authorization=Bearer%20eyJ4NXUiOiJpbXNfbmExLWtleS0xLmNlciIsImFsZyI6IlJTMjU2In0.eyJpZCI6IjE1MTgwMDA4NTExNDZfMmYyODI2ZmUtNTU5MC00MzVlLWJiNDEtNjI2ZTQ2N2ZhZGRjX3VlMSIsImNsaWVudF9pZCI6IkNyZWF0aXZlQ2xvdWRXZWIxIiwidXNlcl9pZCI6IjA1RTMxMzUyNTdGODFDRDQwQTQ5NUU1OEBBZG9iZUlEIiwidHlwZSI6ImFjY2Vzc190b2tlbiIsImFzIjoiaW1zLW5hMSIsImZnIjoiU0ZGUTVSVUZYUFA3T0ZRQUFBQUFBQUFBU0k9PT09PT0iLCJtb2kiOiJiMjU4Y2U0MSIsImMiOiJxR0pzeklRSnBhWG1ncWZCRC9HUUtRPT0iLCJleHBpcmVzX2luIjoiODY0MDAwMDAiLCJzY29wZSI6IkFkb2JlSUQsb3BlbmlkLGduYXYsY3JlYXRpdmVfY2xvdWQscmVhZF9vcmdhbml6YXRpb25zLGFkZGl0aW9uYWxfaW5mby5zY3JlZW5fbmFtZSxhZGRpdGlvbmFsX2luZm8uc2Vjb25kYXJ5X2VtYWlsLGFkZGl0aW9uYWxfaW5mby5yb2xlcyxzYW8uY2Nwcml2YXRlLHNhby5jY19leHRyYWN0LHNhby5jY2VfcHJpdmF0ZSxhY2NvdW50X3R5cGUsc2FvLmNjcHVibGlzaCIsImNyZWF0ZWRfYXQiOiIxNTE4MDAwODUxMTQ2In0.lI2KtI8Kz3xR-oUEu0Gy5LnA6YjeqxHUrmLwhFmx1TGZo425In6Ibco-OzhCQH4Yh8TGvn6m-83F6V0OI1Jj7-gM4kEJIbA9ekKnFq3YuC-2yXZ2tpbc9sIFZQF8rvmi5YJd9dpElbTmbo-8gxAjqQzJaZTIplFQ4DqG-YTz_YHtrCBDSENJt9Pj9bwiDY88HbrfFE91akDsQ2xRBDU5fCncjjAVw-j6PZr6Z57uNOIAFMt_ajPJDyHV6D1OLhj0zcxyGHpBOpJDXTnHTmwJyohizWNE6RS82bkqXy2E75dbzmOYGPpqb_irTPuK-a0nR4x7iwe3TsCr7nVxMnsmKg)

Success!



##Step 2: Install Git

Considering weve had to use Git for FabAcademy for 2 weeks now, It would have been pretty terrible if I didnt have it already!

##Step 3: GitBook
![installing](https://scproxy-prod.adobecc.com/api?X-Location=https%3A%2F%2Fcc-eu1-prod.adobesc.com%2Fapi%2Fv1%2Fassets%2F69355418-baa3-47e8-9a3c-6276c64663d8%2Frenditions%2Fjpg%2F1200?&v=1517969919256&Authorization=Bearer%20eyJ4NXUiOiJpbXNfbmExLWtleS0xLmNlciIsImFsZyI6IlJTMjU2In0.eyJpZCI6IjE1MTgwMDA4NTExNDZfMmYyODI2ZmUtNTU5MC00MzVlLWJiNDEtNjI2ZTQ2N2ZhZGRjX3VlMSIsImNsaWVudF9pZCI6IkNyZWF0aXZlQ2xvdWRXZWIxIiwidXNlcl9pZCI6IjA1RTMxMzUyNTdGODFDRDQwQTQ5NUU1OEBBZG9iZUlEIiwidHlwZSI6ImFjY2Vzc190b2tlbiIsImFzIjoiaW1zLW5hMSIsImZnIjoiU0ZGUTVSVUZYUFA3T0ZRQUFBQUFBQUFBU0k9PT09PT0iLCJtb2kiOiJiMjU4Y2U0MSIsImMiOiJxR0pzeklRSnBhWG1ncWZCRC9HUUtRPT0iLCJleHBpcmVzX2luIjoiODY0MDAwMDAiLCJzY29wZSI6IkFkb2JlSUQsb3BlbmlkLGduYXYsY3JlYXRpdmVfY2xvdWQscmVhZF9vcmdhbml6YXRpb25zLGFkZGl0aW9uYWxfaW5mby5zY3JlZW5fbmFtZSxhZGRpdGlvbmFsX2luZm8uc2Vjb25kYXJ5X2VtYWlsLGFkZGl0aW9uYWxfaW5mby5yb2xlcyxzYW8uY2Nwcml2YXRlLHNhby5jY19leHRyYWN0LHNhby5jY2VfcHJpdmF0ZSxhY2NvdW50X3R5cGUsc2FvLmNjcHVibGlzaCIsImNyZWF0ZWRfYXQiOiIxNTE4MDAwODUxMTQ2In0.lI2KtI8Kz3xR-oUEu0Gy5LnA6YjeqxHUrmLwhFmx1TGZo425In6Ibco-OzhCQH4Yh8TGvn6m-83F6V0OI1Jj7-gM4kEJIbA9ekKnFq3YuC-2yXZ2tpbc9sIFZQF8rvmi5YJd9dpElbTmbo-8gxAjqQzJaZTIplFQ4DqG-YTz_YHtrCBDSENJt9Pj9bwiDY88HbrfFE91akDsQ2xRBDU5fCncjjAVw-j6PZr6Z57uNOIAFMt_ajPJDyHV6D1OLhj0zcxyGHpBOpJDXTnHTmwJyohizWNE6RS82bkqXy2E75dbzmOYGPpqb_irTPuK-a0nR4x7iwe3TsCr7nVxMnsmKg)

It hung for what seemed like an enternity and I truly thought it was just crashed. It finally finished up though!

![Success](https://scproxy-prod.adobecc.com/api?X-Location=https%3A%2F%2Fcc-eu1-prod.adobesc.com%2Fapi%2Fv1%2Fassets%2Fa0f1a02e-45ca-4f3d-bcfc-647108720829%2Frenditions%2Fjpg%2F1200?&v=1517969943161&Authorization=Bearer%20eyJ4NXUiOiJpbXNfbmExLWtleS0xLmNlciIsImFsZyI6IlJTMjU2In0.eyJpZCI6IjE1MTgwMDA4NTExNDZfMmYyODI2ZmUtNTU5MC00MzVlLWJiNDEtNjI2ZTQ2N2ZhZGRjX3VlMSIsImNsaWVudF9pZCI6IkNyZWF0aXZlQ2xvdWRXZWIxIiwidXNlcl9pZCI6IjA1RTMxMzUyNTdGODFDRDQwQTQ5NUU1OEBBZG9iZUlEIiwidHlwZSI6ImFjY2Vzc190b2tlbiIsImFzIjoiaW1zLW5hMSIsImZnIjoiU0ZGUTVSVUZYUFA3T0ZRQUFBQUFBQUFBU0k9PT09PT0iLCJtb2kiOiJiMjU4Y2U0MSIsImMiOiJxR0pzeklRSnBhWG1ncWZCRC9HUUtRPT0iLCJleHBpcmVzX2luIjoiODY0MDAwMDAiLCJzY29wZSI6IkFkb2JlSUQsb3BlbmlkLGduYXYsY3JlYXRpdmVfY2xvdWQscmVhZF9vcmdhbml6YXRpb25zLGFkZGl0aW9uYWxfaW5mby5zY3JlZW5fbmFtZSxhZGRpdGlvbmFsX2luZm8uc2Vjb25kYXJ5X2VtYWlsLGFkZGl0aW9uYWxfaW5mby5yb2xlcyxzYW8uY2Nwcml2YXRlLHNhby5jY19leHRyYWN0LHNhby5jY2VfcHJpdmF0ZSxhY2NvdW50X3R5cGUsc2FvLmNjcHVibGlzaCIsImNyZWF0ZWRfYXQiOiIxNTE4MDAwODUxMTQ2In0.lI2KtI8Kz3xR-oUEu0Gy5LnA6YjeqxHUrmLwhFmx1TGZo425In6Ibco-OzhCQH4Yh8TGvn6m-83F6V0OI1Jj7-gM4kEJIbA9ekKnFq3YuC-2yXZ2tpbc9sIFZQF8rvmi5YJd9dpElbTmbo-8gxAjqQzJaZTIplFQ4DqG-YTz_YHtrCBDSENJt9Pj9bwiDY88HbrfFE91akDsQ2xRBDU5fCncjjAVw-j6PZr6Z57uNOIAFMt_ajPJDyHV6D1OLhj0zcxyGHpBOpJDXTnHTmwJyohizWNE6RS82bkqXy2E75dbzmOYGPpqb_irTPuK-a0nR4x7iwe3TsCr7nVxMnsmKg)

##Step 4: Gitbook content :-D
![Push](https://scproxy-prod.adobecc.com/api?X-Location=https%3A%2F%2Fcc-eu1-prod.adobesc.com%2Fapi%2Fv1%2Fassets%2F708230e2-3dc8-4a49-87c6-4537e99e7dfd%2Frenditions%2Fjpg%2F1200?&v=1517969999844&Authorization=Bearer%20eyJ4NXUiOiJpbXNfbmExLWtleS0xLmNlciIsImFsZyI6IlJTMjU2In0.eyJpZCI6IjE1MTgwMDA4NTExNDZfMmYyODI2ZmUtNTU5MC00MzVlLWJiNDEtNjI2ZTQ2N2ZhZGRjX3VlMSIsImNsaWVudF9pZCI6IkNyZWF0aXZlQ2xvdWRXZWIxIiwidXNlcl9pZCI6IjA1RTMxMzUyNTdGODFDRDQwQTQ5NUU1OEBBZG9iZUlEIiwidHlwZSI6ImFjY2Vzc190b2tlbiIsImFzIjoiaW1zLW5hMSIsImZnIjoiU0ZGUTVSVUZYUFA3T0ZRQUFBQUFBQUFBU0k9PT09PT0iLCJtb2kiOiJiMjU4Y2U0MSIsImMiOiJxR0pzeklRSnBhWG1ncWZCRC9HUUtRPT0iLCJleHBpcmVzX2luIjoiODY0MDAwMDAiLCJzY29wZSI6IkFkb2JlSUQsb3BlbmlkLGduYXYsY3JlYXRpdmVfY2xvdWQscmVhZF9vcmdhbml6YXRpb25zLGFkZGl0aW9uYWxfaW5mby5zY3JlZW5fbmFtZSxhZGRpdGlvbmFsX2luZm8uc2Vjb25kYXJ5X2VtYWlsLGFkZGl0aW9uYWxfaW5mby5yb2xlcyxzYW8uY2Nwcml2YXRlLHNhby5jY19leHRyYWN0LHNhby5jY2VfcHJpdmF0ZSxhY2NvdW50X3R5cGUsc2FvLmNjcHVibGlzaCIsImNyZWF0ZWRfYXQiOiIxNTE4MDAwODUxMTQ2In0.lI2KtI8Kz3xR-oUEu0Gy5LnA6YjeqxHUrmLwhFmx1TGZo425In6Ibco-OzhCQH4Yh8TGvn6m-83F6V0OI1Jj7-gM4kEJIbA9ekKnFq3YuC-2yXZ2tpbc9sIFZQF8rvmi5YJd9dpElbTmbo-8gxAjqQzJaZTIplFQ4DqG-YTz_YHtrCBDSENJt9Pj9bwiDY88HbrfFE91akDsQ2xRBDU5fCncjjAVw-j6PZr6Z57uNOIAFMt_ajPJDyHV6D1OLhj0zcxyGHpBOpJDXTnHTmwJyohizWNE6RS82bkqXy2E75dbzmOYGPpqb_irTPuK-a0nR4x7iwe3TsCr7nVxMnsmKg)

##Step 5: Contents
Something isnt right here, the pages are not linking
![Not linked](https://scproxy-prod.adobecc.com/api?X-Location=https%3A%2F%2Fcc-eu1-prod.adobesc.com%2Fapi%2Fv1%2Fassets%2F79e1e721-e2e1-443d-b408-2fc9a1e77ac6%2Frenditions%2Fjpg%2F500?&v=1517973265178&Authorization=Bearer%20eyJ4NXUiOiJpbXNfbmExLWtleS0xLmNlciIsImFsZyI6IlJTMjU2In0.eyJpZCI6IjE1MTgwMDA4NTExNDZfMmYyODI2ZmUtNTU5MC00MzVlLWJiNDEtNjI2ZTQ2N2ZhZGRjX3VlMSIsImNsaWVudF9pZCI6IkNyZWF0aXZlQ2xvdWRXZWIxIiwidXNlcl9pZCI6IjA1RTMxMzUyNTdGODFDRDQwQTQ5NUU1OEBBZG9iZUlEIiwidHlwZSI6ImFjY2Vzc190b2tlbiIsImFzIjoiaW1zLW5hMSIsImZnIjoiU0ZGUTVSVUZYUFA3T0ZRQUFBQUFBQUFBU0k9PT09PT0iLCJtb2kiOiJiMjU4Y2U0MSIsImMiOiJxR0pzeklRSnBhWG1ncWZCRC9HUUtRPT0iLCJleHBpcmVzX2luIjoiODY0MDAwMDAiLCJzY29wZSI6IkFkb2JlSUQsb3BlbmlkLGduYXYsY3JlYXRpdmVfY2xvdWQscmVhZF9vcmdhbml6YXRpb25zLGFkZGl0aW9uYWxfaW5mby5zY3JlZW5fbmFtZSxhZGRpdGlvbmFsX2luZm8uc2Vjb25kYXJ5X2VtYWlsLGFkZGl0aW9uYWxfaW5mby5yb2xlcyxzYW8uY2Nwcml2YXRlLHNhby5jY19leHRyYWN0LHNhby5jY2VfcHJpdmF0ZSxhY2NvdW50X3R5cGUsc2FvLmNjcHVibGlzaCIsImNyZWF0ZWRfYXQiOiIxNTE4MDAwODUxMTQ2In0.lI2KtI8Kz3xR-oUEu0Gy5LnA6YjeqxHUrmLwhFmx1TGZo425In6Ibco-OzhCQH4Yh8TGvn6m-83F6V0OI1Jj7-gM4kEJIbA9ekKnFq3YuC-2yXZ2tpbc9sIFZQF8rvmi5YJd9dpElbTmbo-8gxAjqQzJaZTIplFQ4DqG-YTz_YHtrCBDSENJt9Pj9bwiDY88HbrfFE91akDsQ2xRBDU5fCncjjAVw-j6PZr6Z57uNOIAFMt_ajPJDyHV6D1OLhj0zcxyGHpBOpJDXTnHTmwJyohizWNE6RS82bkqXy2E75dbzmOYGPpqb_irTPuK-a0nR4x7iwe3TsCr7nVxMnsmKg)

Aha! Summary.md is where the contents get congigured!
```
# Summary

* [Fab Academy 2018](README.md)
  * [Week 1: Principles & Practices](fabacademy/week1.md)
  * [Week 2: Project Management](fabacademy/week2.md)
  * [Week 3: CAD](fabacademy/week3.md)
* [Journal](journal.md)
  * [07/02/2018: My misadventure with GitLab](journal/my-misadventure-with-gitlab.md)
  * [07/02/2018: Bring on the kerf](journal/laserkerf.md)
  * [07/02/2018: Skill Crossover](journal/070218.md)

```

Result!
![Working contents](https://scproxy-prod.adobecc.com/api?X-Location=https%3A%2F%2Fcc-eu1-prod.adobesc.com%2Fapi%2Fv1%2Fassets%2F3a4089f9-cf1d-46fe-93e9-e9b0f7c49670%2Frenditions%2Fjpg%2F500?&v=1517973349212&Authorization=Bearer%20eyJ4NXUiOiJpbXNfbmExLWtleS0xLmNlciIsImFsZyI6IlJTMjU2In0.eyJpZCI6IjE1MTgwMDA4NTExNDZfMmYyODI2ZmUtNTU5MC00MzVlLWJiNDEtNjI2ZTQ2N2ZhZGRjX3VlMSIsImNsaWVudF9pZCI6IkNyZWF0aXZlQ2xvdWRXZWIxIiwidXNlcl9pZCI6IjA1RTMxMzUyNTdGODFDRDQwQTQ5NUU1OEBBZG9iZUlEIiwidHlwZSI6ImFjY2Vzc190b2tlbiIsImFzIjoiaW1zLW5hMSIsImZnIjoiU0ZGUTVSVUZYUFA3T0ZRQUFBQUFBQUFBU0k9PT09PT0iLCJtb2kiOiJiMjU4Y2U0MSIsImMiOiJxR0pzeklRSnBhWG1ncWZCRC9HUUtRPT0iLCJleHBpcmVzX2luIjoiODY0MDAwMDAiLCJzY29wZSI6IkFkb2JlSUQsb3BlbmlkLGduYXYsY3JlYXRpdmVfY2xvdWQscmVhZF9vcmdhbml6YXRpb25zLGFkZGl0aW9uYWxfaW5mby5zY3JlZW5fbmFtZSxhZGRpdGlvbmFsX2luZm8uc2Vjb25kYXJ5X2VtYWlsLGFkZGl0aW9uYWxfaW5mby5yb2xlcyxzYW8uY2Nwcml2YXRlLHNhby5jY19leHRyYWN0LHNhby5jY2VfcHJpdmF0ZSxhY2NvdW50X3R5cGUsc2FvLmNjcHVibGlzaCIsImNyZWF0ZWRfYXQiOiIxNTE4MDAwODUxMTQ2In0.lI2KtI8Kz3xR-oUEu0Gy5LnA6YjeqxHUrmLwhFmx1TGZo425In6Ibco-OzhCQH4Yh8TGvn6m-83F6V0OI1Jj7-gM4kEJIbA9ekKnFq3YuC-2yXZ2tpbc9sIFZQF8rvmi5YJd9dpElbTmbo-8gxAjqQzJaZTIplFQ4DqG-YTz_YHtrCBDSENJt9Pj9bwiDY88HbrfFE91akDsQ2xRBDU5fCncjjAVw-j6PZr6Z57uNOIAFMt_ajPJDyHV6D1OLhj0zcxyGHpBOpJDXTnHTmwJyohizWNE6RS82bkqXy2E75dbzmOYGPpqb_irTPuK-a0nR4x7iwe3TsCr7nVxMnsmKg)

