# Summary

* [Fab Academy 2018](README.md)
  * [Week 1: Principles & Practices](fabacademy/week1.md)
  * [Week 2: Project Management](fabacademy/week2.md)
  * [Week 3: CAD](fabacademy/week3.md)
  * [Week 4: Computter Controlled Cutting](fabacademy/week4.md)
  * [Week 5: Electronics Production](fabacademy/week5.md)
  * [Week 6: 3D Printing and Scanning](fabacademy/week6.md)
  * [Week 7: Electronics Design](fabacademy/week7.md)
  * [Week 8: Embedded Programming](fabacademy/week8.md)
  * [New ISP](newisp.md)

<!--
* [Final Project](fp1.md)
  * [Concept](fabacademy/finalproject/concept.md)
-->
 	


_ _ _
* [Brighton FabAcademy](http://fab.academany.org/2018/labs/fablabbrighton/)
  * [Des](http://fab.academany.org/2018/labs/fablabbrighton/students/derek-covill)
  * [Sophie](http://fab.academany.org/2018/labs/fablabbrighton/students/sophie-kiarie)
  * [Jack](http://fab.academany.org/2018/labs/fablabbrighton/students/jack-lister)
  * [James](http://fab.academany.org/2018/labs/fablabbrighton/students/james-moore)
  * [Paul](http://fab.academany.org/2018/labs/fablabbrighton/students/paul-nichols)
  * [Andrew](http://fab.academany.org/2018/labs/fablabbrighton/students/andrew-sleigh)
  * [Jedd](http://fab.academany.org/2018/labs/fablabbrighton/students/jedd-winterburn)
  * [Mike](https://fa2018.gitlab.io/fablab/)
  * [Alex](https://fa2018.gitlab.io/AlexSite/)
  * [Poppy]()
  * [Donna]()


_ _ _
* [Journal](journal.md)
  * [07/02/2018: Bring on the kerf](journal/laserkerf.md)
  * [07/02/2018: Skill Crossover](journal/070218.md)
  * [08/02/2018: iframe solving](journal/08022018-iframe.md)
  * [08/02/2018: Individual Task Notes](journal/week4-individual-notes.md)
  * [14/02/2018: Week 5 Notes](journal/week5notes.md)
  * [Week 6 notes](journal/week6notes.md)
  * [Week 7 notes](journal/week7notes.md)
_ _ _
* [External]()
  * [Blog](https://blogs.brighton.ac.uk/kjk20)
  * [PD Project 3: Design for the Urban Environment GitBook](https://kjk.designs.gitlab.io/urban/)
  * [GitBook Reference 1](https://bookdown.org/yihui/bookdown/html.html)
  * [Gitbook Reference 2](https://seadude.gitbooks.io/learn-gitbook/content/chapter1/internal.html)


  



